package com.instacart.delivery.ui.fragment.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.instacart.delivery.R
import com.instacart.delivery.ui.activity.base.BaseActivity
import com.instacart.delivery.ui.fragment.orderlist.OrderListFragment
import com.instacart.delivery.utils.AppUtility
import com.instacart.delivery.utils.Screens
import kotlinx.android.synthetic.main.fragment_splash.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.getViewModel

class SplashFragment : Fragment() {

    private lateinit var splashViewModel: SplashViewModel

    private val appUtility: AppUtility by inject()

    companion object {
        fun getInstance(): SplashFragment? {

            return SplashFragment()
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_splash, container, false)
        splashViewModel = getViewModel()
        return view
    }

    override fun onResume() {
        super.onResume()
        (activity as BaseActivity).showToolbarText(false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        splashViewModel.loadingState.observe(this, Observer {
            when (it) {
                Screens.ORDER_LIST.ordinal -> {
                    (activity as BaseActivity).replaceFragment(
                        OrderListFragment.getInstance()!!,
                        false
                    )
                }
            }
        })

        splashViewModel.errorMessage.observe(this, Observer {
            appUtility.showSnackMessage(it, txt_title)
        })

        splashViewModel.successMessage.observe(this, Observer {
            appUtility.showSnackMessage(it, txt_title)
        })
    }


}