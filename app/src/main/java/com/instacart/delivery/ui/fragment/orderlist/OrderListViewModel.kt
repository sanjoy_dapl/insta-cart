package com.instacart.delivery.ui.fragment.orderlist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.femcy.model.interfaces.SelectItem
import com.instacart.delivery.model.entity.Order
import com.instacart.delivery.model.repository.OrderListRepository
import com.instacart.delivery.utils.Screens

class OrderListViewModel(private var orderListRepository: OrderListRepository) : ViewModel(),
    SelectItem {

    val loadingState = MutableLiveData<Int>()

    val resourceMessage: MutableLiveData<Int> = MutableLiveData()
    val textMessage: MutableLiveData<String> = MutableLiveData()

    val orderListAdapter = OrderListAdapter(this)

    var orderId=0

    init {
        val orderList = orderListRepository.getOrderList()
        if (orderList.isNotEmpty()) {
            orderListAdapter.updateList(orderList)
        }
    }

    override fun <T> onSelectItem(t: T) {
        orderId=(t as Order).orderId
        loadingState.value = Screens.ORDER_DETAILS.ordinal
    }

}