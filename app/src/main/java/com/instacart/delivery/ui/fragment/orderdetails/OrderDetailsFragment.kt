package com.instacart.delivery.ui.fragment.orderdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.instacart.delivery.R
import com.instacart.delivery.databinding.FragmentOrderDetailsBinding
import com.instacart.delivery.ui.activity.base.BaseActivity
import com.instacart.delivery.utils.AppUtility
import com.instacart.delivery.utils.ORDER_ID
import com.instacart.delivery.utils.Screens
import kotlinx.android.synthetic.main.fragment_order_list.view.*
import kotlinx.android.synthetic.main.fragment_splash.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.getViewModel

class OrderDetailsFragment : Fragment() {

    private lateinit var orderDetailsViewModel: OrderDetailsViewModel
    private lateinit var binding: FragmentOrderDetailsBinding
    private val appUtility: AppUtility by inject()

    companion object {
        fun getInstance(id: Int): OrderDetailsFragment? {
            val orderDetailsFragment = OrderDetailsFragment()
            val bundle = Bundle()
            bundle.putInt(ORDER_ID, id)
            orderDetailsFragment.arguments = bundle
            return orderDetailsFragment
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_order_details, container, false)
        orderDetailsViewModel = getViewModel()
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        (activity as BaseActivity).showToolbarText(true)
        (activity as BaseActivity).setToolbarText(getString(R.string.orderdetails))
        orderDetailsViewModel.getOrderById(arguments?.getInt(ORDER_ID)!!)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.root.listOrder.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)



        orderDetailsViewModel.resourceMessage.observe(this, Observer {
            appUtility.showSnackMessage(it, binding.root)
        })

        orderDetailsViewModel.textMessage.observe(this, Observer {
            appUtility.showSnackMessage(it, binding.root)
        })

        binding.viewModel = orderDetailsViewModel
    }
}