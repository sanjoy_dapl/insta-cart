package com.instacart.delivery.ui.fragment.orderdetails

import android.text.TextUtils
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.instacart.delivery.model.repository.OrderDetailsRepository
import com.instacart.delivery.utils.roundOffDecimal

class OrderDetailsViewModel(private var orderDetailsRepository: OrderDetailsRepository) :
    ViewModel() {
    val loadingState = MutableLiveData<Int>()

    val resourceMessage: MutableLiveData<Int> = MutableLiveData()
    val textMessage: MutableLiveData<String> = MutableLiveData()


    val totalPrice = MutableLiveData<String>()
    val deliveryTime = MutableLiveData<String>()
    val deliveryAddress = MutableLiveData<String>()
    val storeLocation = MutableLiveData<String>()

    var itemListAdapter = ItemListAdapter()

    fun getOrderById(id: Int) {
        val order = orderDetailsRepository.getOrderItem(id)
        if (order != null) {
            if (!TextUtils.isEmpty(order.deliveryAddress)) {
                deliveryAddress.value = order.deliveryAddress
            }
            if (!TextUtils.isEmpty(order.deliveryTimeWindow)) {
                deliveryTime.value = order.deliveryTimeWindow
            }
            if (!TextUtils.isEmpty(order.storeLocation)) {
                storeLocation.value = order.storeLocation
            }

            if (order.itemList.isNotEmpty()) {
                itemListAdapter.updateList(order.itemList)
                var total = 0.0
                for (item in order.itemList) {
                    total += (item.quantity * item.price)
                }
                totalPrice.value = "$ ${roundOffDecimal(total)}"
            }


        }
    }
}