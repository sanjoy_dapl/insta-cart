package com.instacart.delivery.ui.fragment.orderlist

import android.text.TextUtils
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.instacart.delivery.model.entity.Order
import com.instacart.delivery.utils.roundOffDecimal

class OrderListAdapterViewModel : ViewModel() {

    val itemName = MutableLiveData<String>()
    val itemPrice = MutableLiveData<String>()
    val deliveryTime = MutableLiveData<String>()
    val deliveryAddress = MutableLiveData<String>()

    fun bind(order: Order) {
        if (!TextUtils.isEmpty(order.deliveryAddress)) {
            deliveryAddress.value = order.deliveryAddress
        }
        if (!TextUtils.isEmpty(order.deliveryTimeWindow)) {
            deliveryTime.value = order.deliveryTimeWindow
        }

        if (order.itemList.isNotEmpty()) {
            if (order.itemList.size > 1) {
                if (!TextUtils.isEmpty(order.itemList[0].itemName)) {
                    itemName.value = "${order.itemList[0].itemName} and more.."
                }
            } else {
                if (!TextUtils.isEmpty(order.itemList[0].itemName)) {
                    itemName.value = order.itemList[0].itemName
                }
            }
            var totalPrice = 0.0
            for (item in order.itemList) {
                totalPrice += (item.quantity * item.price)
            }
            itemPrice.value = "$ ${roundOffDecimal(totalPrice)}"

        }

    }
}