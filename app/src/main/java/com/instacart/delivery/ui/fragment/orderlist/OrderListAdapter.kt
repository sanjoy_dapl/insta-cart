package com.instacart.delivery.ui.fragment.orderlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.femcy.model.interfaces.SelectItem
import com.instacart.delivery.R
import com.instacart.delivery.databinding.InflateOrderListItemBinding
import com.instacart.delivery.model.entity.Order

class OrderListAdapter(private var selectItem: SelectItem) :
    RecyclerView.Adapter<OrderListAdapter.ViewHolder>() {

    lateinit var orderList: List<Order>
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: InflateOrderListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.inflate_order_list_item,
            parent,
            false
        )

        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (::orderList.isInitialized) orderList.size else 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(orderList[position])
        holder.itemView.setOnClickListener {
            selectItem.onSelectItem(orderList[position])
        }
    }


    fun updateList(orderList_: List<Order>) {
        if (orderList_.isNotEmpty()) {
            this.orderList = orderList_
            notifyDataSetChanged()
        }
    }

    class ViewHolder(private val binding: InflateOrderListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private val viewModel = OrderListAdapterViewModel()

        fun bind(order: Order) {
            binding.viewModel = viewModel
            viewModel.bind(order)


        }

    }
}