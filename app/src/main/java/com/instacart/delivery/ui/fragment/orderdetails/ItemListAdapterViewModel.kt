package com.instacart.delivery.ui.fragment.orderdetails

import android.text.TextUtils
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.instacart.delivery.model.entity.Item

class ItemListAdapterViewModel : ViewModel() {

    val itemName = MutableLiveData<String>()
    val itemPrice = MutableLiveData<String>()
    val itemQty = MutableLiveData<String>()

    fun bind(item: Item) {
        if (!TextUtils.isEmpty(item.itemName)) {
            itemName.value = item.itemName
        }
        itemPrice.value = "$ ${item.price}"
        itemQty.value = "${item.quantity} X"

    }
}