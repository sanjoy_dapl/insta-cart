package com.instacart.delivery.ui.activity.base

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.instacart.delivery.R
import com.instacart.delivery.ui.fragment.splash.SplashFragment
import kotlinx.android.synthetic.main.activity_base.*

class BaseActivity : AppCompatActivity() {


    private val appInBackground = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        supportFragmentManager.addOnBackStackChangedListener(getListener())
        initView()
    }

    private fun initView() {

        addFragment(SplashFragment.getInstance() as Fragment)

    }

    override fun onBackPressed() {

        if (supportFragmentManager.backStackEntryCount == 1) {

            finish()
        } else {
            super.onBackPressed()
        }
    }

    /**
     * ************************
     * For replace new Fragment
     * ************************
     */
    fun replaceNewFragment(newInstance: Fragment, resId: Int): Boolean {
        return replaceNewFragment(newInstance, resId, false)
    }

    fun replaceFragment(newInstance: Fragment, addToBackStack: Boolean): Boolean {
        return replaceNewFragment(newInstance, R.id.container, addToBackStack)
    }


    /**
     * ********************
     * For add new Fragment
     * ********************
     */
    fun addFragment(newInstance: Fragment): Boolean {
        val fragmentName = newInstance.javaClass.simpleName
        val fragment = supportFragmentManager.findFragmentByTag(fragmentName)
        if (fragment == null) {
            val transaction = supportFragmentManager.beginTransaction()
                .setCustomAnimations(android.R.anim.fade_in, 0)
            transaction.add(R.id.container, newInstance, fragmentName)

            transaction.addToBackStack(fragmentName)
            if (appInBackground)
                transaction.commitAllowingStateLoss()
            else
                transaction.commit()
            return true
        }
        return false
    }

    /**
     * ************************
     * For replace new Fragment
     * ************************
     */
    private fun replaceNewFragment(
        newInstance: Fragment,
        resiId: Int,
        addToBackstack: Boolean
    ): Boolean {
        try {
            val fragmentName = newInstance.javaClass.simpleName
            val fragment = supportFragmentManager.findFragmentByTag(fragmentName)
            return if (fragment == null) {
                val transaction = supportFragmentManager.beginTransaction()
                transaction.setCustomAnimations(android.R.anim.slide_in_left, 0)
                if (addToBackstack) {
                    transaction.addToBackStack(fragmentName)
                }
                transaction.replace(resiId, newInstance, fragmentName)
                if (appInBackground)
                    transaction.commitAllowingStateLoss()
                else
                    transaction.commit()
                true
            } else {
                false
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return false

        }
    }
    private fun getListener(): FragmentManager.OnBackStackChangedListener {

        return FragmentManager.OnBackStackChangedListener {
            val manager = supportFragmentManager

            if (manager != null) {
                val currFrag = manager.findFragmentById(R.id.container)
                currFrag?.onResume()
            }
        }
    }

    fun setToolbarText(str: String) {
        tvToolbarTitle.text = str
    }

    fun showToolbarText(isVisible: Boolean) {
        if (isVisible) {
            tvToolbarTitle.visibility = View.VISIBLE
        } else {
            tvToolbarTitle.visibility = View.GONE
        }
    }
}