package com.instacart.delivery.ui.fragment.orderlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.instacart.delivery.R
import com.instacart.delivery.databinding.FragmentOrderListBinding
import com.instacart.delivery.ui.activity.base.BaseActivity
import com.instacart.delivery.ui.fragment.orderdetails.OrderDetailsFragment
import com.instacart.delivery.utils.AppUtility
import com.instacart.delivery.utils.Screens
import kotlinx.android.synthetic.main.fragment_order_list.view.*
import kotlinx.android.synthetic.main.fragment_splash.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.getViewModel

class OrderListFragment : Fragment() {

    private lateinit var orderListViewModel: OrderListViewModel
    private lateinit var binding: FragmentOrderListBinding
    private val appUtility: AppUtility by inject()

    companion object {
        fun getInstance(): OrderListFragment? {

            return OrderListFragment()
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_order_list, container, false)
        orderListViewModel = getViewModel()
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        (activity as BaseActivity).showToolbarText(true)
        (activity as BaseActivity).setToolbarText(getString(R.string.orderlist))
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.root.listOrder.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        orderListViewModel.loadingState.observe(this, Observer {
            when (it) {
                Screens.ORDER_DETAILS.ordinal -> {
                    (activity as BaseActivity).addFragment(
                        OrderDetailsFragment.getInstance(
                            orderListViewModel.orderId
                        )!!
                    )
                }
            }
        })

        orderListViewModel.resourceMessage.observe(this, Observer {
            appUtility.showSnackMessage(it, binding.root)
        })

        orderListViewModel.textMessage.observe(this, Observer {
            appUtility.showSnackMessage(it, binding.root)
        })

        binding.viewModel = orderListViewModel
    }

}