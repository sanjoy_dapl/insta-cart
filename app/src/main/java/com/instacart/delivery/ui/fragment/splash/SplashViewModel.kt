package com.instacart.delivery.ui.fragment.splash

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.instacart.delivery.R
import com.instacart.delivery.model.repository.SplashRepository
import com.instacart.delivery.utils.Screens
import java.util.*
import kotlin.concurrent.schedule

class SplashViewModel(private var splashRepository: SplashRepository) : ViewModel() {
    val loadingState = MutableLiveData<Int>()

    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val successMessage: MutableLiveData<String> = MutableLiveData()


    init {
        startTimer(2000)
    }

    private fun startTimer(time: Long) {
        Timer("SettingUp", false).schedule(time) {
            onTimerCompleted()
        }
    }

    private fun onTimerCompleted() {
        val orderModel = splashRepository.loadOrderList()
        if (orderModel != null) {
            loadingState.postValue(Screens.ORDER_LIST.ordinal)
        } else {
            errorMessage.postValue(R.string.something_went_wrong_pls_try_again)
        }

    }
}