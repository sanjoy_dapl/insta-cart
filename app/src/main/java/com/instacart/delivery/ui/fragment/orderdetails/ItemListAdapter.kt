package com.instacart.delivery.ui.fragment.orderdetails

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.instacart.delivery.R
import com.instacart.delivery.databinding.InflateItemListItemBinding
import com.instacart.delivery.model.entity.Item

class ItemListAdapter() :
    RecyclerView.Adapter<ItemListAdapter.ViewHolder>() {

    lateinit var itemList: List<Item>
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: InflateItemListItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.inflate_item_list_item,
            parent,
            false
        )

        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (::itemList.isInitialized) itemList.size else 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(itemList[position])

    }


    fun updateList(itemList_: List<Item>) {
        if (itemList_.isNotEmpty()) {
            this.itemList = itemList_
            notifyDataSetChanged()
        }
    }

    class ViewHolder(private val binding: InflateItemListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private val viewModel = ItemListAdapterViewModel()

        fun bind(item: Item) {
            binding.viewModel = viewModel
            viewModel.bind(item)


        }

    }
}