package com.instacart.delivery.utils

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.instacart.delivery.model.entity.OrderModel
import org.koin.android.ext.android.inject

class AppPreference(private var context: Context, private var application: Application) {
    /**
     * Get Json File From Asset Folder
     */
    fun getJsonFileFromAsset(filename: String): String {
        return application.assets.open(filename).bufferedReader().use {
            it.readText()
        }

    }

    /**
     * Shared Preferences Data
     */
    private val preferences: SharedPreferences by application.inject()

    fun saveOrderList(orderDetails: OrderModel) {
        val prefsEditor = preferences.edit()
        val gson = Gson()
        val json = gson.toJson(orderDetails)
        prefsEditor.putString(KEY_ORDER_LIST, json)
        prefsEditor.apply()
    }

    /**
     * Get Order List
     */
    fun getOrderDetails(): OrderModel? {
        val mPrefs = preferences
        val gson = Gson()
        val json = mPrefs.getString(KEY_ORDER_LIST, "")
        return gson.fromJson<OrderModel>(json, OrderModel::class.java)
    }

    /**
     * Delete Order List
     */
    fun deleteOrderDetails() {
        val prefsEditor = preferences.edit()
        val gson = Gson()
        val json = gson.toJson(null)
        prefsEditor.putString(KEY_ORDER_LIST, json)
        prefsEditor.apply()
    }

    /**
     * ********************************
     * END OF Shared Preferences Data
     * ********************************
     */


}