package com.instacart.delivery.utils

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.instacart.delivery.R
import org.jetbrains.anko.design.snackbar

class AppUtility(private var context: Context) {


    /**
     * ********************************
     * Show snackbar message for alerts
     * USE - ANKO LAYOUT
     * ********************************
     */
    fun showSnackMessage(string: String, view: View) {
        if (context != null) {
            view.snackbar(string).setActionTextColor(
                ContextCompat.getColor(
                    context,
                    R.color.md_white_1000
                )
            )
                .view.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
        }

    }

    fun showSnackMessage(@StringRes string: Int, view: View) {
        if (context != null) {
            view.snackbar(string).setActionTextColor(
                ContextCompat.getColor(
                    context,
                    R.color.md_white_1000
                )
            )
                .view.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
        }

    }

    /**
     * ********************************
     * Hide soft key board
     * ********************************
     */
    fun hideSoftKeyboard(view: View) {
        if (view != null && context != null) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }




}