package com.femcy.model.interfaces

interface SelectItem {

    fun <T> onSelectItem(t: T)
}