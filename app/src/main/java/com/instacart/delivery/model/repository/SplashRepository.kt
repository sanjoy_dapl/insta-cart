package com.instacart.delivery.model.repository

import com.google.gson.Gson
import com.instacart.delivery.model.entity.OrderModel
import com.instacart.delivery.utils.ASSET_ORDER_LIST_FILE_NAME
import com.instacart.delivery.utils.AppPreference

class SplashRepository(var appPreference: AppPreference) {

    fun loadOrderList(): OrderModel {
        var orderModel: OrderModel? = null
        try {

            if (appPreference.getOrderDetails() != null) {
                orderModel = appPreference.getOrderDetails()
            } else {
                val json = appPreference.getJsonFileFromAsset(ASSET_ORDER_LIST_FILE_NAME)
                orderModel = Gson().fromJson(json, OrderModel::class.java)
                appPreference.saveOrderList(orderModel)
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return orderModel!!
    }

}