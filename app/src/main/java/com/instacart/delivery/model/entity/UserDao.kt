package com.instacart.delivery.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "order_entity")
data class OrderEntity(
    @PrimaryKey var id: Int,
    var symptomsNote: String,
    var isSymptomsAdded: Boolean? = false
)

@Entity(tableName = "product_entity")
data class ProductEntity(
    @PrimaryKey var id: Int,
    var name: String,
    var price: String
)

@Entity(tableName = "product_order")
data class ProductOrderEntity(
    @PrimaryKey var id: Int,
    var orderId: Int,
    var productId: Int,
    var quantity: String
)


data class OrderModel(
    @SerializedName("message")
    var message: String,
    @SerializedName("order_list")
    var orderList: List<Order>
)

data class Order(
    @SerializedName("delivery_address")
    var deliveryAddress: String,
    @SerializedName("delivery_time_window")
    var deliveryTimeWindow: String,
    @SerializedName("item_list")
    var itemList: List<Item>,
    @SerializedName("order_id")
    var orderId: Int,
    @SerializedName("store_location")
    var storeLocation: String
)

data class Item(
    @SerializedName("item_name")
    var itemName: String,
    @SerializedName("price")
    var price: Double,
    @SerializedName("quantity")
    var quantity: Int
)