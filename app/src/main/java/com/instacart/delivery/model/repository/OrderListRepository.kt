package com.instacart.delivery.model.repository

import com.instacart.delivery.model.entity.Order
import com.instacart.delivery.utils.AppPreference

class OrderListRepository(var appPreference: AppPreference) {

    fun getOrderList(): List<Order> {
        return appPreference.getOrderDetails()?.orderList!!
    }
}