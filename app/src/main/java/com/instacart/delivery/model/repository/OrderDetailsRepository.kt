package com.instacart.delivery.model.repository

import com.instacart.delivery.model.entity.Order
import com.instacart.delivery.utils.AppPreference

class OrderDetailsRepository(var appPreference: AppPreference) {

    fun getOrderItem(id: Int): Order {
        return appPreference.getOrderDetails()?.orderList?.find { order -> order.orderId == id }!!
    }
}