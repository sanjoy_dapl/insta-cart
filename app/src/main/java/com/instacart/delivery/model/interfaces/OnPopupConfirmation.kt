package com.femcy.model.interfaces

interface OnPopupConfirmation {
    fun popupConfirmEvent()
}