package com.instacart.delivery.app

import android.content.Context
import android.content.SharedPreferences
import com.instacart.delivery.model.repository.OrderDetailsRepository
import com.instacart.delivery.model.repository.OrderListRepository
import com.instacart.delivery.model.repository.SplashRepository
import com.instacart.delivery.ui.fragment.orderdetails.OrderDetailsViewModel
import com.instacart.delivery.ui.fragment.orderlist.OrderListViewModel
import com.instacart.delivery.ui.fragment.splash.SplashViewModel
import com.instacart.delivery.utils.AppPreference
import com.instacart.delivery.utils.AppUtility
import com.instacart.delivery.utils.PREFS_FILENAME
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val applicationModule = module(override = true) {
    single<SharedPreferences> {
        androidContext().getSharedPreferences(
            PREFS_FILENAME,
            Context.MODE_PRIVATE
        )
    }
    single { AppUtility(androidContext()) }
    single { AppPreference(androidContext(), androidApplication()) }
}


val viewModelModule = module {
    viewModel { SplashViewModel(get()) }
    viewModel { OrderListViewModel(get()) }
    viewModel { OrderDetailsViewModel(get()) }
}

val repositoryModule = module {
    fun provideSplashRepository(appPreference: AppPreference): SplashRepository {
        return SplashRepository(appPreference)
    }

    fun provideOrderListRepository(appPreference: AppPreference): OrderListRepository {
        return OrderListRepository(appPreference)
    }
    fun provideOrderDetailsRepository(appPreference: AppPreference): OrderDetailsRepository {
        return OrderDetailsRepository(appPreference)
    }

    single { provideSplashRepository(get()) }
    single { provideOrderListRepository(get()) }
    single { provideOrderDetailsRepository(get()) }
}